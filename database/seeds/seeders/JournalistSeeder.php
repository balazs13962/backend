<?php

use Illuminate\Database\Seeder;

use App\Journalist;
use Faker\Factory;

class JournalistSeeder extends Seeder
{
    private $faker;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->faker = Faker\Factory::create();
        $i = 1;

        while ($i++ <= 50) {

            Journalist::updateOrCreate(
                ['id' => $i],
                $this->getRandomJournalDetails()
            );
        }
    }

    /**
     * Return with user array.
     *
     * @return array
     */
    public function getRandomJournalDetails()
    {
        return [
            'name' => $this->faker->name,
            'alias' => $this->faker->secondaryAddress
        ];
    }
}
