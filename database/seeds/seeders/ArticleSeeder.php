<?php

use Illuminate\Database\Seeder;

use App\Article;
use App\Journalist;

class ArticleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$this->faker = Faker\Factory::create();
        $i = 1;

        while ($i++ <= 100) {

        	Article::updateOrCreate(
        		['id' => $i],
                $this->getRandomArticelDetails()
        	);
        }
    }


    /**
     * Return with Article array.
     *
     * @return array
     */
    protected function getRandomArticelDetails()
    {
    	return [
    		'title' => $this->faker->jobTitle,
    		'path' => $this->faker->uuid,
    		'text' => $this->faker->realText,
    		'journalist_id' => Journalist::all()->random()->id,
    	];
    }
}
