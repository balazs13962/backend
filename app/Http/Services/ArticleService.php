<?php

namespace App\Http\Services;

use App\Article;
use App\Http\Requests\Article\ValidateAttributes;

class ArticleService
{

	/**
	 * instance
     * Return instance.
     *
     * @return App\Http\Services\ArticleService
     */
	public static function instance()
	{
		return (new ArticleService());
	}

	/**
	 * store
     * Create one Article.
     *
     * @param App\Http\Requests\Article\ValidateAttributes
     * @return App\Article
     */
	public function store(ValidateAttributes $request)
	{
		Article::create($request->all());
	}

	/**
	 * titles
     * Return collection with all titles.
     *
     * @return collection
     */
	public function titles()
	{
		$titles = collect();
		foreach (Article::all() as $article) {
			$titles->push($article->path);
		}
		return $titles;
	}

	/**
	 * getPath
     * Return one Article object.
     *
     * @return App\Article
     */
	public function getPath($pathName)
	{
		return Article::where('path', $pathName)->firstOrFail();
	}


}