<?php

namespace App\Http\Services;

use App\Journalist;

class JournalistService
{

	/**
	 * instance
     * Return instance of JournalistService class.
     *
     * @return App\Http\Services\JournalistService
     */
	public static function instance()
	{
		return (new JournalistService());
	}


	/**
	 * getAll
     * Return collection with all journalist.
     *
     * @return collection
     */
	public function getAll()
	{
		$journalists = collect();
		foreach (Journalist::all() as $journalist) {

			$journalists->push([
				"Name" => $journalist->name,
				"Alias" => $journalist->alias
			]);
		}
		
		return $journalists;
	}

	/**
	 * getOne
     * Return Journalist object.
     *
     * @return App\Journalist
     */
	public function getOne($alias)
	{
		return Journalist::where('alias', $alias)->firstOrFail();
	}

	/**
	 * storeOne
     * Store the Journalist.
     *
     * @return boolean
     */
	public function storeOne($request)
	{
		Journalist::updateOrCreate(
                ['alias' => $request->alias],
                $request->all()
            );
	}
}