<?php

namespace App\Http\Middleware\Journalists;

use Closure;
use App\Journalist;

class CheckAliasIsExist
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        Journalist::where('alias', $request->route('alias'))->firstOrFail();

        return $next($request);
    }
}
