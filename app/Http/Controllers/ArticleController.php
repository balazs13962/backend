<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\Article\ValidateAttributes;
use App\Http\Services\ArticleService;

class ArticleController extends Controller
{
    
	/**
	 * titles
     * Return collection with all titles.
     *
     * @return collection
     */
    public function titles()
    {
    	return ArticleService::instance()->titles();
    }

	/**
	 * store
     * Create one Article.
     *
     * @param App\Http\Requests\Article\ValidateAttributes
     * @return App\Article
     */
    public function store(ValidateAttributes $request)
    {
    	return ArticleService::instance()->store($request);
    }

	/**
	 * getPath
     * Return one Article object.
     *
     * @return App\Article
     */
    public function path($pathName)
    {
    	return ArticleService::instance()->getPath($pathName);
    }
}
