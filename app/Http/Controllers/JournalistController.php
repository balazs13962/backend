<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Services\JournalistService;
use App\Http\Requests\Journalists\ValidateAttributes;

class JournalistController extends Controller
{
    
	/**
	 * index
     * Return collection with all journalist.
     *
     * @return collection
     */
	public function index()
	{
		return JournalistService::instance()->getAll();
	}

	/**
	 * getOne
     * Return Journalist object.
     *
     * @return App\Journalist
     */
	public function journalist($alias)
	{
		return JournalistService::instance()->getOne($alias);
	}

	public function store(ValidateAttributes $journalist)
	{
		return JournalistService::instance()->storeOne($journalist);
	}

}
