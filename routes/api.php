<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



Route::group(['prefix'=>'journalists'], function () {
    
    Route::get('', 'JournalistController@index');
	Route::post('', 'JournalistController@store');
	Route::get('{alias}', 'JournalistController@journalist')->middleware('journalist_check_alias_exist');
});

Route::group(['prefix'=>'articles'], function () {
    
    Route::get('titles', 'ArticleController@titles');
	Route::post('', 'ArticleController@store');
	Route::get('{path}', 'ArticleController@path');
});
